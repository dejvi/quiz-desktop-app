﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsGUI
{
    public class Results
    {
        public string Question { get; set; }

        public string correct_answer { get; set; }

        public List<string> incorrect_answers { get; set; }

    }
}
