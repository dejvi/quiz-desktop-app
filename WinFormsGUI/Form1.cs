﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using Newtonsoft.Json.Linq;
using static System.Console;

namespace WinFormsGUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ApiHelper.InitializeClient();
        }

        private async void submitButton_Click(object sender, EventArgs e)
        {
            var q = await ApiProcessor.LoadQuiz(9, "Easy", "Multiple");
            foreach (Results a in q)
            {
                MessageBox.Show(a.Question);
                MessageBox.Show(a.correct_answer);
            }
        }

    }
}
